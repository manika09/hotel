package com.agoda.hotel.service.api;

/**
 * @author manika.singh
 * @since 12/02/20
 */
public interface RateLimiterService {

  boolean isValidRequest(String uri, Long timeLimit, int requestLimit, Long pause);
}
