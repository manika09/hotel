package com.agoda.hotel.service.api;

import com.agoda.hotel.util.Pageable;
import com.agoda.hotel.model.Hotel;

import java.util.List;

/**
 * @author manika.singh
 * @since 08/02/20
 */
public interface HotelService {

  /**
   * Find hotels by city
   * @param city city name
   * @param pageable pagination object reference
   * @return list of hotels in given city
   */
  List<Hotel> findByCity(String city, Pageable pageable);

  /**
   * Find hotels by room type
   * @param roomType type of room
   * @param pageable agination object reference
   * @return list of hotels in having given roomType
   */
  List<Hotel> findByRoomType(String roomType, Pageable pageable);
}
