package com.agoda.hotel.service.impl;

import au.com.bytecode.opencsv.CSVReader;
import com.agoda.hotel.exception.ErrorCode;
import com.agoda.hotel.exception.HotelException;
import com.agoda.hotel.util.Constant;
import com.agoda.hotel.util.Pageable;
import com.agoda.hotel.util.Utility;
import com.agoda.hotel.model.Hotel;
import com.agoda.hotel.service.api.HotelService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//1 . extract common fuctionality, 2. Write test
// 3. Try if data can be stored constantly 4. update config file 5. make exception package
// 6. cache 7.logging

/**
 * @author manika.singh
 * @since 08/02/20
 */
@Service
public class HotelServiceImpl implements HotelService {

  private static final List<Hotel> hotels = initialize();

  private static List<Hotel> initialize() {

    List<Hotel> hotelList = new ArrayList<>();
    Path currentRelativePath = Paths.get("");
    String path = currentRelativePath.toAbsolutePath().toString();
    path = path + ("/src/main/resources/hoteldb.csv");
    CSVReader reader = null;
    try {
      reader = new CSVReader(new FileReader(path));
      String[] columnRecord = reader.readNext();
      while ((columnRecord = reader.readNext()) != null) {
        Hotel hotel = new Hotel();
        hotel.setCity(columnRecord[0]);
        hotel.setId(Integer.parseInt(columnRecord[1]));
        hotel.setRoomType(columnRecord[2]);
        hotel.setPrice(Double.parseDouble(columnRecord[3]));
        hotelList.add(hotel);
      }

    } catch (IOException e) {
      e.getMessage();
    }
    return hotelList;
  }

  public List<Hotel> findByCity(String city, Pageable pageable) {
    if (StringUtils.isEmpty(city)) {
      throw new HotelException(ErrorCode.INVALID_INPUT);
    }

    List<Hotel> cityHotel = new ArrayList<>();
    hotels.forEach(hotel -> {
      if (hotel.getCity().equalsIgnoreCase(city)) {
        cityHotel.add(hotel);
      }
    });

    if (pageable == null) {
      return cityHotel;
    }

    if (pageable.getSortDir().equals(Constant.SORT_DIR_ASC)) {
      Collections.sort(cityHotel, (h1, h2) -> (int) (h1.getPrice() - h2.getPrice()));
    } else {
      Collections.sort(cityHotel, (h1, h2) -> (int) (h2.getPrice() - h1.getPrice()));
    }

    return Utility.getPage(cityHotel, pageable.getPageNum(), pageable.getPageSize());
  }

  public List<Hotel> findByRoomType(String roomType, Pageable pageable) {
    if (StringUtils.isEmpty(roomType)) {
      throw new HotelException(ErrorCode.INVALID_INPUT);
    }
    List<Hotel> cityHotel = new ArrayList<>();
    hotels.forEach(hotel -> {
      if (hotel.getRoomType().equalsIgnoreCase(roomType)) {
        cityHotel.add(hotel);
      }
    });

    if (pageable == null) {
      return cityHotel;
    }

    if (pageable.getSortDir().equals(Constant.SORT_DIR_ASC)) {
      Collections.sort(cityHotel, (h1, h2) -> (int) (h1.getPrice() - h2.getPrice()));
    } else {
      Collections.sort(cityHotel, (h1, h2) -> (int) (h2.getPrice() - h1.getPrice()));
    }

    return Utility.getPage(cityHotel, pageable.getPageNum(), pageable.getPageSize());
  }

}
