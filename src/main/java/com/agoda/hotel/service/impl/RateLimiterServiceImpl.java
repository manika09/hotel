package com.agoda.hotel.service.impl;

import com.agoda.hotel.service.api.RateLimiterService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author manika.singh
 * @since 12/02/20
 */
@Service
public class RateLimiterServiceImpl implements RateLimiterService {

  private static Map<String, PriorityQueue<Long>> rateMap = new ConcurrentHashMap<>();
  private static Map<String, Long> thresholdMap = new ConcurrentHashMap<>();

  @Override
  public boolean isValidRequest(String uri, Long timeLimit, int requestLimit, Long pause) {

    Long currTime = System.currentTimeMillis();
    PriorityQueue<Long> queue = rateMap.get(uri);
    Long thresholdLimit = thresholdMap.get(uri);
    if (thresholdLimit != null && currTime - thresholdLimit < pause) {
      return false;
    }
    if (rateMap.containsKey(uri)) {

      PriorityQueue<Long> pq = queue;
      if (pq.size() < requestLimit) {
        pq.add(currTime);
      } else if (pq.size() == requestLimit) {
        if (currTime - pq.peek() > timeLimit) {
          pq.poll();
          pq.add(currTime);
        } else {
          thresholdMap.put(uri, currTime);
          return false;
        }
      }
    } else {
      PriorityQueue<Long> pq = new PriorityQueue<>();
      pq.add(currTime);
      rateMap.put(uri, pq);
    }
    return true;
  }
}
