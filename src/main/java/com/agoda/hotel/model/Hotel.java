package com.agoda.hotel.model;

import java.util.Comparator;

/**
 * @author manika.singh
 * @since 08/02/20
 */
public class Hotel {

  private int id;
  private String city;
  private String roomType;
  private Double price;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getRoomType() {
    return roomType;
  }

  public void setRoomType(String roomType) {
    this.roomType = roomType;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public class CompareByPrice implements Comparator<Hotel> {

    @Override
    public int compare(Hotel o1, Hotel o2) {
      return (int) (o1.getPrice()-o2.getPrice());
    }
  }
}
