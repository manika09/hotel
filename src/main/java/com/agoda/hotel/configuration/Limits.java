package com.agoda.hotel.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author manika.singh
 * @since 13/02/20
 */
@ConfigurationProperties
public class Limits {

  private Long timeLimit;
  private int maxRequest;
  private Long pauseTime;

  public Long getTimeLimit() {
    return timeLimit;
  }

  public void setTimeLimit(Long timeLimit) {
    this.timeLimit = timeLimit;
  }

  public int getMaxRequest() {
    return maxRequest;
  }

  public void setMaxRequest(int maxRequest) {
    this.maxRequest = maxRequest;
  }

  public Long getPauseTime() {
    return pauseTime;
  }

  public void setPauseTime(Long pauseTime) {
    this.pauseTime = pauseTime;
  }
}
