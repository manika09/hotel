package com.agoda.hotel.configuration;

import com.agoda.hotel.service.api.RateLimiterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author manika.singh
 * @since 14/02/20
 */
@Component
public class RateLimitInterceptor implements HandlerInterceptor {

  private static final Long DEFAULT_LIMIT = 50L;
  private static final int DEFAULT_SIZE = 10;
  private static final Long DEFAULT_PAUSE = 5000L;

  @Autowired
  Properties properties;

  @Autowired
  RateLimiterService rateLimiterService;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                           Object handler) {
    String uri = request.getRequestURI();
    uri = normalize(uri);
    Limits limits = properties.getConfig().get(uri);
    Long timeLimit;
    int requestLimit;
    Long pause;
    if (limits != null) {
      timeLimit = limits.getTimeLimit();
      requestLimit = limits.getMaxRequest();
      pause = limits.getPauseTime();
    } else {
      timeLimit = DEFAULT_LIMIT;
      requestLimit = DEFAULT_SIZE;
      pause = DEFAULT_PAUSE;
    }

    Boolean valid = rateLimiterService.isValidRequest(uri, timeLimit, requestLimit, pause);
    if (!valid) {
      response.setStatus(HttpStatus.TOO_MANY_REQUESTS.value());
    }
    return valid;
  }

  private String normalize(String uri) {
    String[] split = uri.trim().split("/");
    if (split.length > 1) {
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < split.length - 1; i++) {
        if (!split[i].equals("")) {
          sb.append(split[i]).append("-");
        }
      }
      sb.append(split[split.length - 1]);
      return sb.toString();
    }
    return uri;
  }
}
