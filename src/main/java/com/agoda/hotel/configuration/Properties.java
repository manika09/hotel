package com.agoda.hotel.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * @author manika.singh
 * @since 13/02/20
 */
@EnableAutoConfiguration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "properties")
public class Properties {

  public Map<String, Limits> config = new HashMap<>();

  public Map<String, Limits> getConfig() {
    return config;
  }

  public void setConfig(Map<String, Limits> config) {
    this.config = config;
  }
}
