package com.agoda.hotel.util;

/**
 * @author manika.singh
 * @since 16/02/20
 */
public class URI {
  public static final String HOTEL_BASE_PATH = "/hotel";
  public static final String CITY = "/city";
  public static final String ROOM_TYPE = "/room";
}
