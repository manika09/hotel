package com.agoda.hotel.util;

/**
 * @author manika.singh
 * @since 08/02/20
 */
public class Constant {

  public static final String SORT_DIR_ASC = "ASC";
  public static final String SORT_DIR_DESC = "DESC";
}
