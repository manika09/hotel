package com.agoda.hotel.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author manika.singh
 * @since 08/02/20
 */
public class Utility {

  public static <T> List<T> getPage(List<T> list, int pageNum, int pageSize) {
    List<T> result = new ArrayList<>();

    int totalSize = list.size();
    int totPages = (int)Math.ceil(totalSize/pageSize);

    if(pageNum <= totPages) {
      int start = pageNum * pageSize;
      int end = Math.min(start+pageSize, totalSize);
      result = list.subList(start, end);
    }
    return result;
  }
}
