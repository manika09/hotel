package com.agoda.hotel.util;

import java.io.Serializable;

/**
 * @author manika.singh
 * @since 08/02/20
 */
public class Pageable implements Serializable {
  private int pageNum;
  private int pageSize;
  private int totalRecords;
  private String sortDir;

  public Pageable() {

  }
  public Pageable(int pageNum, int pageSize, String sortDir) {
    this.pageNum = pageNum;
    this.pageSize = pageSize;
    this.sortDir = sortDir;
  }

  public int getPageNum() {
    return pageNum;
  }

  public void setPageNum(int pageNum) {
    this.pageNum = pageNum;
  }

  public int getPageSize() {
    return pageSize;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }

  public int getTotalRecords() {
    return totalRecords;
  }

  public void setTotalRecords(int totalRecords) {
    this.totalRecords = totalRecords;
  }

  public String getSortDir() {
    return sortDir;
  }

  public void setSortDir(String sortDir) {
    this.sortDir = sortDir;
  }
}
