package com.agoda.hotel.controller;

import com.agoda.hotel.exception.HotelException;
import com.agoda.hotel.model.Hotel;
import com.agoda.hotel.service.api.HotelService;
import com.agoda.hotel.util.Pageable;
import com.agoda.hotel.util.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author manika.singh
 * @since 08/02/20
 */
@RestController
@RequestMapping(value = URI.HOTEL_BASE_PATH)
public class HotelController {

  @Autowired
  private HotelService hotelService;

  @RequestMapping(value = URI.CITY, method = RequestMethod.GET, produces =
      MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<Hotel>> findByCity(@RequestParam String city,
                                                @RequestBody(required = false) Pageable pageable) {

    List<Hotel> hotels;
    try {
      hotels = this.hotelService.findByCity(city, pageable);
    } catch (HotelException he) {
      return new ResponseEntity(he.getErrorCode().getStatusCode());
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return new ResponseEntity<>(hotels, HttpStatus.OK);

  }

  @RequestMapping(value = URI.ROOM_TYPE, method = RequestMethod.GET, produces =
      MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<Hotel>> findByRoomType(@RequestParam String roomType,
                                                    @RequestBody(required = false) Pageable pageable) {
    List<Hotel> hotels;
    try {
      hotels = this.hotelService.findByRoomType(roomType, pageable);
    } catch (HotelException he) {
      return new ResponseEntity(he.getErrorCode().getStatusCode());
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return new ResponseEntity<>(hotels, HttpStatus.OK);
  }

  @RequestMapping(value = "/test", method = RequestMethod.GET, produces =
      MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> test() {
    return new ResponseEntity("ok", HttpStatus.OK);
  }
}
