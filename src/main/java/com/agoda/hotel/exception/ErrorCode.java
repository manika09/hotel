package com.agoda.hotel.exception;

import org.springframework.http.HttpStatus;

/**
 * @author manika.singh
 * @since 15/02/20
 */
public enum ErrorCode {

  TOO_MANY_REQUESTS(HttpStatus.TOO_MANY_REQUESTS, "Limit exceeded for number of request. Please try in sometime!"),
  INVALID_INPUT(HttpStatus.BAD_REQUEST, "Invalid input parameters");

  ErrorCode(HttpStatus statusCode, String message) {
    this.statusCode = statusCode;
    this.message = message;
  }

  private HttpStatus statusCode;
  private String message;

  public String getMessage() {
    return message;
  }

  public HttpStatus getStatusCode() {
    return statusCode;
  }
}
