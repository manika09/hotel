package com.agoda.hotel.exception;

/**
 * @author manika.singh
 * @since 12/02/20
 */
public class HotelException extends RuntimeException {

  private final ErrorCode errorCode;

  public HotelException(ErrorCode errorCode) {
    this.errorCode = errorCode;
  }


  public ErrorCode getErrorCode() {
    return errorCode;
  }

  @Override
  public String getMessage() {
    return this.errorCode.getMessage();
  }

}
