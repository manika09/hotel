package com.agoda.hotel;

import com.agoda.hotel.controller.HotelController;
import com.agoda.hotel.model.Hotel;
import com.agoda.hotel.service.api.HotelService;
import com.agoda.hotel.util.Constant;
import com.agoda.hotel.util.Pageable;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

/**
 * @author manika.singh
 * @since 16/02/20
 */
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
@TestPropertySource(
    locations = "classpath:application-test.properties")
public class HotelControllerTest {

  private static final String CITY = "Bangkok";
  private static final String ROOM_TYPE = "Deluxe";

  @InjectMocks
  private HotelController hotelController;

  @Mock
  HotelService hotelService;

  private MockMvc mockMvc;
  private Hotel hotel1;
  private Hotel hotel2;
  private Hotel hotel3;
  private List<Hotel> hotelList;
  private Pageable pageable;

  @Before
  public void setUp() {
    initMocks(this);
    this.mockMvc = MockMvcBuilders.standaloneSetup(this.hotelController).build();
    hotel1 = new Hotel();
    hotel1.setId(1);
    hotel1.setCity("Bangkok");
    hotel1.setPrice(10000D);
    hotel1.setRoomType("Deluxe");

    hotel2 = new Hotel();
    hotel2.setId(2);
    hotel2.setCity("Bangkok");
    hotel2.setPrice(2000D);
    hotel2.setRoomType("Superior");

    hotel3 = new Hotel();
    hotel3.setId(3);
    hotel3.setCity("Amsterdam");
    hotel3.setPrice(3000D);
    hotel3.setRoomType("Superior");

    hotelList = new ArrayList<>();
    hotelList.add(hotel1);
    hotelList.add(hotel2);
    hotelList.add(hotel3);

    pageable = new Pageable();
    pageable.setPageNum(0);
    pageable.setPageSize(1);
    pageable.setSortDir(Constant.SORT_DIR_ASC);

  }

  @Test
  public void findByCityTestSuccessPageableNullTest() throws Exception {

    when(hotelService.findByCity(CITY, null)).thenReturn(hotelList);

    this.mockMvc.perform(get("/hotel/city")
        .contentType(MediaType.APPLICATION_JSON_VALUE).param("city", CITY))
        .andExpect(status().isOk());
    verify(this.hotelService).findByCity(CITY, null);
  }


  @Test
  public void findByCityTestSuccessAscTest() throws Exception {

    when(hotelService.findByCity(eq(CITY), any(Pageable.class))).thenReturn(hotelList);

    this.mockMvc.perform(get("/hotel/city")
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .param("city", CITY)
        .content(new ObjectMapper().writeValueAsString(pageable)))
        .andExpect(status().isOk());
    verify(this.hotelService).findByCity(eq(CITY), any(Pageable.class));
  }

  @Test
  public void findByRoomTestSuccessPageableNullTest() throws Exception {

    when(hotelService.findByRoomType(ROOM_TYPE, null)).thenReturn(hotelList);

    this.mockMvc.perform(get("/hotel/room")
        .contentType(MediaType.APPLICATION_JSON_VALUE).param("roomType", ROOM_TYPE))
        .andExpect(status().isOk());
    verify(this.hotelService).findByRoomType(ROOM_TYPE, null);
  }


  @Test
  public void findByRoomTestSuccessAscTest() throws Exception {

    when(hotelService.findByRoomType(eq(ROOM_TYPE), any(Pageable.class))).thenReturn(hotelList);

    this.mockMvc.perform(get("/hotel/room")
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .param("roomType", ROOM_TYPE)
        .content(new ObjectMapper().writeValueAsString(pageable)))
        .andExpect(status().isOk());
    verify(this.hotelService).findByRoomType(eq(ROOM_TYPE), any(Pageable.class));
  }

  @After
  public void tearDown() {
    verifyNoMoreInteractions(hotelService);
  }
}
