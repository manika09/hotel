# Hotel API Rate limiter

The project is aimed at implementing rate limiter for specific APIs.

## Build

To build the application JDK 1.8 is required. Go to hotel directory and run the following command


```bash
mvn clean install
```
## Run

To run and deploy the application run the following command :

```bash
java -jar target/hotel-1.0.0.jar
```
Embedded tomcat will start running at port 8080
## Implementation

The rate limit functionality is as follows :

There are three configurable parameters for rate-limiting, they have been accessed by means of class Limits.

timeLimit represents time in milliseconds permitted for each maxRequests.
PauseTime represents the buffer time for which the request stops responding.

The configuration per API can be added in application.properties file in the format 

properties.config.${api-path}.timeLimit

properties.config.${api-path}.maxRequest

properties.config.${api-path}.pauseTime


Although the data should be stored and retrieved from the database, for the sake of simplicity and the main aim being developing rate limiting service,
I am processing and constructing hotel list in memory from the CSV file provided instead of storing the hotel information in the database

For rate-limiting functionality, I have created a request interceptor to validate if the request can be made or not. I have used a queue of size maximum request allowed within a given time frame, and storing incoming request timestamps in it. On each request, If the number of requests allowed reached the threshold, the timestamp inserted first in the queue is checked to determine if the maximum request limit is exceeded.

If it is exceeded, I am storing the last limit exceeded timestamp in a map of API and last threshold reaching timestamp. This value will be used later to not allow further requests to go through for a certain amount of time. 

The APIs can be accessed using following curl commands :
```curl

curl --location --request GET 'http://localhost:8080/agoda/hotel/city?city=Bangkok' \
--header 'Content-Type: application/json' \
--data-raw '{
  "pageNum": 0,
  "pageSize": 10,
  "sortDir": "DESC"
}'


curl --location --request GET 'http://localhost:8080/agoda/hotel/room?roomType=Deluxe' \
--header 'Content-Type: application/json' \
--data-raw '{
  "pageNum": 0,
  "pageSize": 10,
  "sortDir": "ASC"
}'
```